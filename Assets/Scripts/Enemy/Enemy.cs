using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CameraSpace;

public class Enemy : MonoBehaviour
{
    //Health
    private float hitpoints = 100.0f;
    [SerializeField] Canvas canvas;
    [SerializeField] Slider slider;

    //Scaracter controller
    [SerializeField] CharacterController cc;

    //Animator
    [SerializeField] Animator anim;

    //RagDoll
    [SerializeField] Rigidbody[] all_rb;
    [SerializeField] Collider[] all_cols;

    [Header("VALUES")]
    [Header("WATCH")]
    //watch
    float watchDistance = 25.0f;
    public LayerMask mask;
    public LayerMask colisionMask;

    Vector3 posOfset = new Vector3(0, 0.75f, 0);

    [Header("MOVE")]
    public bool isMooving = false;
    private Transform player =null;
    Vector3 playerDirection;
    float walkSpeed = 2.5f;
    float runSpeed = 10.0f;
    public bool wayIsClear = true;
    int wayChanged = 1;

    [Header("ATACK")]
    [SerializeField] Transform atackPoint;
    [SerializeField] Transform chest;

    List<Vector3> atackVectors = new List<Vector3>(3) { Vector3.forward, Vector3.up, Vector3.right};
    float atackDamage = 10.0f;
    float distance = 0.4f;

    bool punchReady = true;
    bool atackActive = false;



    private EnemyAnimations enemyAnimations;

    /// <summary>
    /// 0 = wait; 1 = idle; 2 = walk; 3 = run; 4 =atack;
    /// </summary>
    private int status = 0;

    private void Awake()
    {
        InitializeEnemy();
    }

    private void OnEnable()
    {
        InitializeEnemy();
        status = 1;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }



    //ACTION BY ENEMY`S STATUS
    private void Update()
    {
        switch (status)
        {
            case 0:
                break;

            case 1:
                WatchForPlayer();

                break;

            case 2:
                WatchForPlayer();
                ColisionDetect();
                break;

            case 3:
                //ColisionDetect();
                HuntForPlayer();
                break;
            case 4:
                Atack();
                break;
        }

        if(status>0) Mover();
    }

    private void SwitchStatus(int newStatus)
    {
        StopAllCoroutines();
        //Debug.Log("Switch to " + newStatus);
        status = newStatus;
        switch (newStatus)
        {
            case 0:
                isMooving = false;
                anim.SetBool("walk", false);
                anim.SetBool("run", false);
                break;

            case 1:
                isMooving = false;
                anim.SetBool("walk", false);
                anim.SetBool("run", false);
                StartCoroutine(NewDirection());
                break;

            case 2:
                isMooving = true;
                anim.SetBool("walk", true);
                anim.SetBool("run", false);
                StartCoroutine(Patrol());
                break;
            case 3:
                enemyAnimations.Scream();
                anim.SetBool("run", true);
                //anim.SetBool("walk", false);
                wayIsClear = true;
                isMooving = true;
                break;
            case 4:
                isMooving = false;
                anim.SetBool("walk", false);
                anim.SetBool("run", false);
                anim.SetTrigger("atack");
                punchReady = true;
                break;
        }
    }



    //initialize
    private void InitializeEnemy()
    {
        //drop player 
        player = null;

        //turn off ragdoll
        RagdollAble(false);


        //init camera for canvas rendering
        StartCoroutine(InitCameraUpdate());

        //set 
        slider.maxValue = hitpoints;
        slider.value = hitpoints;
        canvas.enabled = true;
        wayChanged = 1;

        //can punch 
        punchReady = true;

        //initialize enemy animations
        if (enemyAnimations == null) enemyAnimations = GetComponent<EnemyAnimations>();

        StartCoroutine(NoDirectionPatrol());

    }
    private IEnumerator InitCameraUpdate()
    {
        while (PlayerCameras.instance == null) yield return null;
        PlayerCameras.instance.set_fvp.AddListener(CanvasCameraUpdate);
        PlayerCameras.instance.set_tvp.AddListener(CanvasCameraUpdate);

    }
    private void CanvasCameraUpdate()
    {
        if (PlayerCameras.instance != null)
        {
            canvas.worldCamera = PlayerCameras.instance.activeCamera;
        }

        else
        {
            StartCoroutine(InitCameraUpdate());
        }
    }

    //switchers
    private void RagdollAble(bool setAble)
    {
        anim.enabled = !setAble;
        cc.enabled = !setAble;
        foreach (Rigidbody rb in all_rb) rb.isKinematic = !setAble;
        foreach (Collider c in all_cols) c.enabled = setAble;
    }
    public void GetDamage(float damage, Vector3 hitPos)
    {
        slider.value -= damage;
        if (slider.value <= 0)
        {
            StopAllCoroutines();
            StartCoroutine(onDeath(hitPos, damage));
        }
        else
        {
            playerDirection = (hitPos - transform.position).normalized;
            playerDirection.y = 0;
            transform.forward = playerDirection;
        }

    }
    private IEnumerator onDeath(Vector3 forcePoint, float damage)
    {
        status = -1;


        isMooving = false;
        enemyAnimations.OnDeath();

        RagdollAble(true);
        canvas.enabled = false;

        float force = (damage * 100);

        foreach (Rigidbody rb in all_rb)
        {
            rb.AddExplosionForce(force, forcePoint, 10.0f);
        }
        yield return new WaitForSeconds(5.0f);
        EnemyPool.instance.BackToPool(this.gameObject);
    }




    private void Mover()
    {
        Vector3 move = Vector3.zero;
        float speed = 0;

        if (isMooving)
        {
            if (player == null) speed = walkSpeed;

            else
            {
                if (wayIsClear)
                {
                    playerDirection = (player.position - transform.position).normalized;
                    playerDirection.y = 0;
                    transform.forward = playerDirection;
                }


                speed = runSpeed;
            }
        }
        move += (transform.forward*speed);
        cc.SimpleMove(move);

    }

    private void WatchForPlayer()
    {
        Vector3 newPos = transform.position + posOfset;
        Vector3 ofset = new Vector3(0, 0, 0);

        //draw multiple rays for finding player
        for(float i = -1.0f; i<=1.0f; i += 0.25f)
        {
            ofset.x = i;

            Vector3 direction = (transform.forward + ofset).normalized;
            //Debug.DrawRay(newPos, direction * watchDistance, Color.red);

            Ray ray = new Ray(newPos, direction);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 15.0f, mask))
            {
                SwitchStatus(3);
                player = hit.collider.gameObject.transform;
            }
        }
    }

    private void ColisionDetect()
    {
        Vector3 newPos = transform.position + posOfset;
        Vector3 ofset = new Vector3(0, 0, 0);
        for (float i = -1.0f; i <= 1.0f; i += 0.25f)
        {
            ofset.x = i;

            Vector3 direction = (transform.forward + ofset).normalized;
            Ray ray = new Ray(newPos, direction);
            RaycastHit hit;

            Debug.DrawRay(newPos, direction * 0.5f, Color.green);
            if (Physics.Raycast(ray, out hit, 0.5f, colisionMask))
            {

                //Debug.Log("colision with " + hit.collider.gameObject.name);
                if (status < 3)
                {
                    SwitchStatus(1);
                }

                else
                {
                    NewWay();
                }

            }
        }
    }

    

    private IEnumerator NewDirection()
    {
        float waitTime = Random.Range(3.0f, 7.5f);

        yield return new WaitForSeconds(waitTime);

        float newX = Random.Range(-1, 1);
        float newZ = Random.Range(-1, 1);
        Vector3 newDir = new Vector3(newX, 0, newZ);
        Vector3 ofset = new Vector3(0, 0, 0);

        Vector3 newPos = transform.position + posOfset;
        Ray ray = new Ray(newPos, newDir);
        RaycastHit hit;
        if (Physics.Raycast(ray, 5.0f, colisionMask))
        {
            float farRecord = 0;
            for (float i = -1.0f; i <= 1.0f; i += 0.25f)
            {
                ofset.x = i;

                Vector3 direction = (transform.forward + ofset).normalized;

                Ray ray2 = new Ray(newPos, direction);
                RaycastHit hit2;

                if (Physics.Raycast(ray2, out hit2, colisionMask))
                {
                    if (hit2.distance > farRecord)
                    {
                        farRecord = hit2.distance;
                        newDir = direction;
                    }
                }
            }
        }

        for (float pol = 0; pol < 1.0f; pol += Time.deltaTime)
        {
            Vector3 dir = Vector3.Lerp(transform.forward, newDir, pol);
            transform.forward = dir;
            yield return null;
        }

        yield return null;
        SwitchStatus(2);

    }

    private void NewWay()
    {
        Debug.Log("new way");
        wayIsClear = false;
        StopAllCoroutines();

        Vector3 newPos = transform.position + posOfset;
        newPos.y = cc.stepOffset;
        Vector3 ofset = new Vector3(0, 0, 0);

        Vector3 nextForward = -transform.forward;
        float farRecord = 0;


        for (float i = -1.0f; i <= 1.0f; i += 0.25f)
        {
            ofset.x = i;

            Vector3 direction = (transform.forward + ofset).normalized;
            Debug.DrawRay(newPos, direction * watchDistance, Color.red);

            Ray ray = new Ray(newPos, direction);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.distance > farRecord)
                {
                    farRecord = hit.distance;
                    nextForward = direction;
                }
            }
        }
        transform.forward = nextForward;

        wayChanged++;
        StartCoroutine(NewWayFor(0.5f * wayChanged));
    }

    private IEnumerator NewWayFor(float f)
    {
        yield return new WaitForSeconds(f);
        wayIsClear = false;

    }


    private void HuntForPlayer()
    {
        Vector3 pos = chest.position;
        Vector3 dir = transform.forward;
        Ray ray = new Ray(pos, dir);
        RaycastHit hit;

        Debug.DrawRay(pos, dir, Color.red);

        if (Physics.Raycast(ray, out hit, 1.0f, mask))
        {
            SwitchStatus(4);

        }
    }




    public void StartAtackZone()
    {
        atackActive = true;
    }

    public void StopAtackZone()
    {
        atackActive = false;
    }

    public void AtackAnimOver()
    {
        Vector3 pos = chest.position;
        Vector3 dir = transform.forward;
        Ray ray = new Ray(pos, dir);
        RaycastHit hit;

        Debug.DrawRay(pos, dir, Color.red);

        if (Physics.Raycast(ray, out hit, 1.0f, mask))
        {
            SwitchStatus(4);

        }
        else SwitchStatus(3);


    }

    private void Atack()
    {
        if (atackActive)
        {
            atackVectors[0] = atackPoint.forward;
            atackVectors[1] = atackPoint.up;
            atackVectors[2] = atackPoint.right;

            for (int i = 0; i < atackVectors.Count; i++)
            {
                Vector3 pos = atackPoint.position;
                Vector3 dir = atackVectors[i];
                Ray ray = new Ray(pos, dir);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, distance, mask))
                {
                    PunchPlayer(player.gameObject.GetComponent<PlayerHelth>());
                }
            }
        }

    }

    private void PunchPlayer(PlayerHelth player)
    {
        if (punchReady)
        {
            player.Damaged(atackDamage);
            punchReady = false;
        }

    }

    public void StartPatrol()
    {
        SwitchStatus(2);
    }

    public void WalkAtDirection(Transform tr)
    {
        Vector2 dir = (tr.position - transform.position).normalized;
        dir.y = 0;
        transform.forward = dir;
        SwitchStatus(2);

    }

    private IEnumerator Patrol()
    {
        float walkTime = Random.Range(5.5f, 10.0f);
        yield return new WaitForSeconds(walkTime);

        //Debug.Log("Patrol is finished");
        if (status == 2) SwitchStatus(1);
        else Debug.Log("WRONG status = " + status);


    }

    private IEnumerator NoDirectionPatrol()
    {
        yield return new WaitForSeconds(2.5f);
        if (status == 0) SwitchStatus(2);
    }
}


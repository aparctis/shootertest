using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPool : MonoBehaviour
{
    private List<GameObject> enemy_pool = new List<GameObject>();
    [SerializeField] GameObject enemy_pfb;

    public static EnemyPool instance;

    private void Awake()
    {
        if (instance == null) instance = this;

    }

    public void BackToPool(GameObject killedEnemy)
    {
        killedEnemy.SetActive(false);
        enemy_pool.Add(killedEnemy);
    }

    public GameObject newEnemyObj()
    {
        GameObject enemyObj;

        if (enemy_pool.Count > 0)
        {
            enemyObj = enemy_pool[0];
            enemy_pool.RemoveAt(0);
        }

        else
        {
            enemyObj = Instantiate(enemy_pfb) as GameObject;
            enemyObj.SetActive(false);
        }

        return enemyObj;
    }
}

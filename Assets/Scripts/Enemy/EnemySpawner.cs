using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] Transform[] spawnPlaces;

    bool spawnAbe = true;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player"&&spawnAbe)
        {
            Debug.Log("SPAWN ENEMYS");
            spawnAbe = false;
            foreach(Transform tr in spawnPlaces)
            {
                GameObject enemy = EnemyPool.instance.newEnemyObj();
                enemy.transform.position = tr.position;
                enemy.SetActive(true);
            }
        }
    }

}

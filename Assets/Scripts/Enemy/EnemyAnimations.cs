using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyAnimations : MonoBehaviour
{
    [SerializeField] AudioSource steps_audio;
    [SerializeField] AudioSource voice_audio;

    [Header("SOUNDS")]
    [Space]
    [Header("STEPS")]
    [SerializeField] List<AudioClip> step_sounds = new List<AudioClip>();
    int lastStepID = 0;

    [Space]
    [Header("VOICE")]
    [SerializeField] List<AudioClip> calm_voice = new List<AudioClip>();
    [SerializeField] List<AudioClip> atack_screams = new List<AudioClip>();
    [SerializeField] AudioClip agression_scream;

    private bool isCalm = true;

    private void OnEnable()
    {
        isCalm = true;
    }

    private void Update()
    {
        if (isCalm && !voice_audio.isPlaying)
        {
            int id = Random.Range(0, calm_voice.Count);
            voice_audio.clip = calm_voice[id];
            voice_audio.Play();
        }
    }

    public void PlayCalm()
    {
        isCalm = true;
    }


    public void StepSound()
    {
        lastStepID++;
        if (lastStepID >= step_sounds.Count) lastStepID = 0;
        steps_audio.clip = step_sounds[lastStepID];
        steps_audio.Play();
    }

    public void PunchSound()
    {
        int id = Random.Range(0, atack_screams.Count);
        voice_audio.clip = atack_screams[id];
        voice_audio.Play();
    }

    public void Scream()
    {
        isCalm = false;
        voice_audio.clip = agression_scream;
        voice_audio.Play();
    }

    public void OnDeath()
    {
        steps_audio.Stop();
        voice_audio.Stop();
    }

}

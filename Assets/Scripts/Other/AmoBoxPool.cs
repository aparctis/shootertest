using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmoBoxPool : MonoBehaviour
{
    [Header("SPAWN POSITIONS")]
    [SerializeField] List<Transform> all_spawn_places = new List<Transform>();
    private List<BoxPlace> all_places = new List<BoxPlace>();
    private List<BoxPlace> empty_places = new List<BoxPlace>();
    private List<GameObject> box_pool = new List<GameObject>();

    [Header("BOX PREFAB")]
    [SerializeField] GameObject box_pfb;


    [SerializeField] float minDelay;
    [SerializeField] float maxDelay;

    public static AmoBoxPool instance;

    private void Awake()
    {
        if (instance == null) instance = this;
        for(int i = 0; i<all_spawn_places.Count; i++)
        {
            BoxPlace newPlace = new BoxPlace(all_spawn_places[i].position, i);
            all_places.Add(newPlace);
        }

        StartCoroutine(SpawCicle());
    }

    public void BackToPool(GameObject box, int id)
    {
        box.SetActive(false);
        box_pool.Add(box);
        all_places[id].SetWithBox(false);
    }

    private GameObject newBox()
    {
        GameObject ob;
        if (box_pool.Count > 0)
        {
            ob = box_pool[0];
            box_pool.RemoveAt(0);
        }
        else
        {
            ob = Instantiate(box_pfb) as GameObject;
            ob.SetActive(false);
        }

        return ob;
    }

    private void Spawn()
    {
        //Debug.Log("Spawn try");
        empty_places.Clear();
        foreach(BoxPlace place in all_places)
        {
            if (!place.withBox) empty_places.Add(place);
        }

        if (empty_places.Count > 0)
        {
            GameObject go = newBox();
            AmoBox box = go.GetComponent<AmoBox>();
            int randomEmpty = Random.Range(0, empty_places.Count);
            BoxPlace place = empty_places[randomEmpty];
            go.transform.position = place.place;
            place.SetWithBox(true);
            box.SetId(place.id);
            go.SetActive(true);
        }
    }

    private IEnumerator SpawCicle()
    {
        while (true)
        {
            float delay = Random.Range(minDelay, maxDelay);
            yield return new WaitForSeconds(delay);
            Spawn();
        }
    }

    public class BoxPlace
    {
        public Vector3 place { get; private set; }
        public bool withBox { get; private set; }
        public int id { get; private set; }
        public BoxPlace (Vector3 v, int i)
        {
            place = v;
            withBox = false;
            id = i;
        }

        public void SetWithBox(bool b)
        {
            withBox = b;

        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenu : MonoBehaviour
{
    [SerializeField] Transform camera;

    /// <summary>
    /// 0 = start, 1 = options, 2 = keys, 3 = win/lose
    /// </summary>
    [SerializeField] List<Transform> cameraPos = new List<Transform>();
    int curentPos = 0;

    //0 - play, 1 - options, 2 - exit
    [SerializeField] List<Button> all_uiButtons = new List<Button>();


    [Header("GRAVE-1")]
    //0 - back, 1 - stats, 2 - keys
    [SerializeField]
    [Header("0 - back, 1 - stats, 2 - keys")]

    List<MenuButton> grave_1 = new List<MenuButton>();

    [Header("GRAVE-2")]
    [SerializeField] List<MenuButton> grave_2 = new List<MenuButton>();

    [Header("GRAVE-3")]
    [SerializeField] List<MenuButton> grave_3 = new List<MenuButton>();

    private List<List<MenuButton>> all_graves = new List<List<MenuButton>>();

    [SerializeField] TextMeshProUGUI win_text;
    [SerializeField] TextMeshProUGUI lose_text;

    [SerializeField] Image loadPage;
    [SerializeField] Image loadImage;
    [SerializeField] TextMeshProUGUI loadText;
    private Color invisible = new Color(0, 0, 0, 0);

    [Header("AUDIO")]
    [SerializeField] AudioSource music;
    [SerializeField] AudioSource click_sound;

    private void Awake()
    {
        StartCoroutine(LoaderOnStart());
    }
    private void Start()
    {
        all_graves.Add(grave_1);
        all_graves.Add(grave_2);
        all_graves.Add(grave_3);

        ButtonInit(grave_1);
        ButtonInit(grave_2);
        ButtonInit(grave_3);

        int loses = PlayerPrefs.GetInt("lose");
        int wins = PlayerPrefs.GetInt("win");

        win_text.text = wins.ToString();
        lose_text.text = loses.ToString();

        camera.position = cameraPos[0].position;

        all_uiButtons[0].onClick.AddListener(PlayGame);
        all_uiButtons[1].onClick.AddListener(Options);
        all_uiButtons[2].onClick.AddListener(ExitGame);
    }

    private void ButtonInit(List<MenuButton> list)
    {
        list[0].onClick.AddListener(Back);
        if (list.Count == 3)
        {
            list[1].onClick.AddListener(Keys);
            list[2].onClick.AddListener(Stats);
        }

        foreach (MenuButton mb in list) mb.SetInterectuble(false);
    }




    private void Back()
    {
        StartCoroutine(SwitchToPosition(0));
    }
    private void Options()
    {
        StartCoroutine(SwitchToPosition(1));

    }

    private void Keys()
    {
        StartCoroutine(SwitchToPosition(2));

    }
    private void Stats()
    {
        StartCoroutine(SwitchToPosition(3));

    }





    private IEnumerator UIButtonAble(bool setAble)
    {
        foreach(Button button in all_uiButtons)
        {
            TextMeshProUGUI buttonText = button.GetComponentInChildren<TextMeshProUGUI>();
            if (setAble)
            {
                for (float f = 0; f < 1.0f; f += Time.deltaTime*4)
                {
                    Color newBlack = Color.Lerp(invisible, Color.black, f);
                    Color newWhite = Color.Lerp(invisible, Color.white, f);
                    button.image.color = newWhite;
                    buttonText.color = newBlack;
                    yield return null;
                }
                button.image.color = Color.white;
                buttonText.color = Color.black;
                button.interactable = true;
            }

            else
            {
                button.interactable = false;

                for (float f = 0; f < 1.0f; f += Time.deltaTime * 4)
                {
                    Color newBlack = Color.Lerp(Color.black, invisible, f);
                    Color newWhite = Color.Lerp(Color.white, invisible, f);
                    button.image.color = newWhite;
                    buttonText.color = newBlack;
                    yield return null;
                }
                button.image.color = invisible;
                buttonText.color = invisible;
            }

        }



    }

    private IEnumerator SwitchToPosition(int posID)
    {
        click_sound.Play();
        foreach (List<MenuButton> list in all_graves)
        {
            foreach (MenuButton mb in list) mb.SetInterectuble(false);
        }
        foreach (Button b in all_uiButtons) b.interactable = false;
        if (curentPos == 0) StartCoroutine(UIButtonAble(false));

        Vector3 start = cameraPos[curentPos].position;
        Vector3 target = cameraPos[posID].position;

        for (float f = 0; f < 1.0f; f += Time.deltaTime)
        {
            Vector3 newPos = Vector3.Lerp(camera.position, target, f);
            camera.position = newPos;
            yield return null;

        }

        curentPos = posID;
        if (curentPos == 0) StartCoroutine(UIButtonAble(true));
        else
        {
            List<MenuButton> list = all_graves[posID - 1];
            foreach (MenuButton mb in list) mb.SetInterectuble(true);

        }

    }


    private void PlayGame()
    {
        click_sound.Play();
        StartCoroutine(AsynkLevelLoader(1));
    }

    private void ExitGame()
    {
        click_sound.Play();
        Application.Quit();
    }

    private IEnumerator AsynkLevelLoader(int level)
    {
        foreach(List<MenuButton> list in all_graves)
        {
            foreach (MenuButton mb in list) mb.SetInterectuble(false);
        }
        foreach (Button b in all_uiButtons) b.interactable = false;

        AsyncOperation sceneLoader = SceneManager.LoadSceneAsync(level);
        sceneLoader.allowSceneActivation = false;

        loadImage.color = invisible;
        loadPage.color = invisible;
        loadText.color = invisible;

        for (float f = 0; f < 1.0f; f += Time.deltaTime)
        {
            Color newBlack = Color.Lerp(invisible, Color.black, f);
            Color newWhite = Color.Lerp(invisible, Color.white, f);
            loadPage.color = newBlack;
            loadImage.color = newWhite;
            loadText.color = newWhite;
            yield return null;
        }

        for (float f = 0; f < 1.0f; f += Time.deltaTime)
        {
            Color newWhite = Color.Lerp(Color.white, invisible, f);
            loadImage.color = newWhite;
            loadText.color = newWhite;
            yield return null;
        }

        sceneLoader.allowSceneActivation = true;

    }
    private IEnumerator LoaderOnStart()
    {
        loadPage.color = Color.black;

        for (float f = 0; f < 1.0f; f += Time.deltaTime)
        {
            Color newWhite = Color.Lerp(Color.black, invisible, f);
            loadPage.color = newWhite;
            yield return null;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateOpener : MonoBehaviour
{
    [Header("SWITCHER")]
    [SerializeField] Transform switcher;

    [Header("GATE")]
    [SerializeField] GameObject gate;
    [SerializeField] Transform gate_downPos;

    [Header("CANVAS WITH ADVICE")]
    [SerializeField] private GameObject canvas;

    bool playerTrigered = false;
    bool canTurnOn = true;

    [Header("AUDIO")]
    [SerializeField] AudioSource source_switcher;
    [SerializeField] AudioSource source_Gates;
    [SerializeField] float openTime;

    void Update()
    {
        if (playerTrigered&&canTurnOn)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                StartCoroutine(Open());
            }
        }
    }

    private IEnumerator Open()
    {
        canTurnOn = false;
        canvas.SetActive(false);

        source_switcher.Play();
        while (source_switcher.isPlaying)
        {
            switcher.Rotate(1,0,0);
            yield return null;
        }

        yield return new WaitForSeconds(0.1f);
        source_Gates.Play();
        Vector3 gatesStartPos = new Vector3(gate.transform.position.x, gate.transform.position.y, gate.transform.position.z);
        Vector3 gatesNewPos = gate_downPos.position;
        float f = 0;
        while (f < 1.0)
        {
            f += (Time.deltaTime / openTime);
            gate.transform.position = Vector3.Lerp(gatesStartPos, gatesNewPos, f);
            yield return null;
        }
        yield return new WaitForSeconds(0.5f);
        if (source_Gates.isPlaying) source_Gates.Stop();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player"&&canTurnOn)
        {
            playerTrigered = true;
            canvas.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player" && canTurnOn)
        {
            playerTrigered = false;
            canvas.SetActive(false);
        }
    }
}

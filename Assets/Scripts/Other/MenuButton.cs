using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(MeshRenderer))]
public class MenuButton : MonoBehaviour
{
    public bool interectuble { get; private set; }

    [SerializeField] Color normalColor = Color.white;
    [SerializeField] Color highlightedColor = Color.white;
    [SerializeField] Color selectedColor = Color.white;
    [SerializeField] Color disabledColor = Color.white;


    public UnityEvent onClick;

    private Material mat;

    private void Start()
    {
        mat = GetComponent<MeshRenderer>().material;
        mat.color = normalColor;
        interectuble = true;
    }

    private void OnMouseDown()
    {

        if (interectuble) mat.color = selectedColor;

    }

    private void OnMouseUp()
    {
        if (interectuble)
        {
            mat.color = normalColor;
            onClick.Invoke();
            interectuble = false;
        }
    }

    private void OnMouseEnter()
    {
        if (interectuble) mat.color = highlightedColor;
    }

    private void OnMouseExit()
    {
        if (interectuble) mat.color = normalColor;

    }

    public void SetInterectuble(bool isInterectuble)
    {
        if (mat == null)
        {
            mat = GetComponent<MeshRenderer>().material;

        }
        if (isInterectuble == true)
        {
            mat.color = normalColor;
        }

        else
        {
            mat.color = disabledColor;
        }
        interectuble = isInterectuble;
    }


}

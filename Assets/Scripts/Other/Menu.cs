using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class Menu : MonoBehaviour
{
    [SerializeField] bool withCursor = false;
    int win;
    int lose;
    [Header("PAGES")]
    [SerializeField] GameObject page_win;
    [SerializeField] GameObject page_lose;
    [SerializeField] GameObject page_pausa;
    private bool isOnPausa = false;

    [Header("PLAYER")]
    [SerializeField] PlayerHelth playerHealth;


    [Header("PLAYER")]
    [SerializeField] GameObject page_loader;
    [SerializeField] Image loadPage;
    [SerializeField] Image loadImage;
    [SerializeField] TextMeshProUGUI loadText;
    Color invisible = new Color(0, 0, 0, 0);

    [Header("PLAYER")]
    [SerializeField] Button[] restartButtons;
    [SerializeField] Button[] exitButtons;
    [SerializeField] Button[] goMenuButtons;
    [SerializeField] Button continueButton;

    [Header("MUSIC")]
    [SerializeField] List<AudioClip> musicClips = new List<AudioClip>();
    [SerializeField] Slider volumeSlider;
    AudioSource musicSource;

    private void Awake()
    {
        StartCoroutine(LoaderOnStart());
    }
    private void Start()
    {
        Cursor.visible = withCursor;
        win = PlayerPrefs.GetInt("win");
        lose = PlayerPrefs.GetInt("lose");
        playerHealth.onLevelWin.AddListener(Win);
        playerHealth.onPlayersDeath.AddListener(Lose);

        foreach (Button button in restartButtons) button.onClick.AddListener(Restart);
        foreach (Button button in exitButtons) button.onClick.AddListener(ExitGame);
        foreach (Button button in goMenuButtons) button.onClick.AddListener(GoMenu);
        continueButton.onClick.AddListener(PausaBreak);

        musicSource = GetComponent<AudioSource>();
        volumeSlider.maxValue = 1.0f;
        volumeSlider.value = musicSource.volume;
        volumeSlider.onValueChanged.AddListener(ChangeVolume);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isOnPausa) PausaBreak();
            else PausaStart();
        }

        if (!musicSource.isPlaying || musicSource.clip==null)
        {
            int rdm = Random.Range(0, musicClips.Count);
            musicSource.clip = musicClips[rdm];
            musicSource.Play();
        }
    }

    private void ChangeVolume(float newVolume)
    {
        musicSource.volume = newVolume;
    }

    private void Win()
    {
        win++;
        PlayerPrefs.SetInt("win", win);
        page_win.SetActive(true);
        Cursor.visible = true;
    }

    private void Lose()
    {
        lose++;
        PlayerPrefs.SetInt("lose", lose);
        page_lose.SetActive(true);
        Cursor.visible = true;

    }

    private void PausaStart()
    {
        Time.timeScale = 0;
        Cursor.visible = true;
        page_pausa.SetActive(true);
        isOnPausa = true;
    }

    private void PausaBreak()
    {
        Time.timeScale = 1.0f;
        page_pausa.SetActive(false);
        Cursor.visible = false;
        isOnPausa = false;
    }

    private void Restart()
    {
        Time.timeScale = 1.0f;
        int levelID = SceneManager.GetActiveScene().buildIndex;
        StartCoroutine(AsynkLevelLoader(levelID));
    }

    private void GoMenu()
    {
        Time.timeScale = 1.0f;
        StartCoroutine(AsynkLevelLoader(0));
    }

    public void ExitGame()
    {
        Application.Quit();
    }


    private IEnumerator AsynkLevelLoader(int level)
    {
        AsyncOperation sceneLoader = SceneManager.LoadSceneAsync(level);
        sceneLoader.allowSceneActivation = false;
        Debug.Log("Next level is" + level);

        page_lose.SetActive(false);
        page_pausa.SetActive(false);
        page_win.SetActive(false);

        loadImage.color = invisible;
        loadPage.color = invisible;
        loadText.color = invisible;

        page_loader.SetActive(true);

        for (float f = 0; f < 1.0f; f += Time.deltaTime)
        {
            Color newBlack = Color.Lerp(invisible, Color.black, f);
            Color newWhite = Color.Lerp(invisible, Color.white, f);
            loadPage.color = newBlack;
            loadImage.color = newWhite;
            loadText.color = newWhite;
            yield return null;
        }

        for (float f = 0; f < 1.0f; f += Time.deltaTime)
        {
            Color newWhite = Color.Lerp( Color.white, invisible,f);
            loadImage.color = newWhite;
            loadText.color = newWhite;
            yield return null;
        }

        sceneLoader.allowSceneActivation = true;

    }

    private IEnumerator LoaderOnStart()
    {
        loadPage.color = Color.black;

        page_loader.SetActive(true);
        for (float f = 0; f < 1.0f; f += Time.deltaTime)
        {
            Color newWhite = Color.Lerp(Color.black, invisible, f);
            loadPage.color = newWhite;
            yield return null;
        }
        page_loader.SetActive(false);


    }
}

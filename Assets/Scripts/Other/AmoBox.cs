using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmoBox : MonoBehaviour
{
    List<WeapoonType> type = new List<WeapoonType>() { WeapoonType.ak, WeapoonType.shotgun, WeapoonType.smg };

    public int place_id { get; private set; }
    private bool willReturn = false;

    private void OnDisable()
    {
        StopAllCoroutines();
        if (willReturn)
        {
            AmoBoxPool.instance.BackToPool(this.gameObject, place_id);
        }
    }

    private void OnEnable()
    {
        StartCoroutine(Rotator());
        StartCoroutine(Floater());
    }

    public void SetId(int i)
    {
        place_id = i;
        willReturn = true;
    }

    private IEnumerator Rotator()
    {
        while (true)
        {
            transform.Rotate(0, 0.5f, 0);
            yield return null;
        }
    }

    private IEnumerator Floater()
    {
        float ofsetY = 0.2f;
        float speed = 1.0f;
        Vector3 downPos = new Vector3(transform.position.x, (transform.position.y - ofsetY), transform.position.z);
        Vector3 upPos = new Vector3(transform.position.x, (transform.position.y + ofsetY), transform.position.z);
        while (true)
        {
            while (transform.position != downPos)
            {
                transform.position = Vector3.MoveTowards(transform.position, downPos, speed * Time.deltaTime);
                yield return null;
            }
            while (transform.position != upPos)
            {
                transform.position = Vector3.MoveTowards(transform.position, upPos, speed * Time.deltaTime);
                yield return null;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {

            if (other.gameObject.GetComponent<GunManager>() != null)
            {

                int rdm = Random.Range(0, type.Count);
                int count = Random.Range(5, 40);
                WeapoonType t = type[rdm];
                other.gameObject.GetComponent<GunManager>().AddAmo(t, count);

                transform.gameObject.SetActive(false);
            }
        }
    }
}

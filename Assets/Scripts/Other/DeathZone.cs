using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("IN LAVA - " + other.gameObject.name);
        if (other.gameObject.GetComponent<PlayerHelth>() != null)
        {
            other.gameObject.GetComponent<PlayerHelth>().Damaged(1000.0f);
        }
    }
}

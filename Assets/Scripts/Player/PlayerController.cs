using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private CharacterController cc;

    [Header("MOOVE")]

    [Header("SPEED")]
    [SerializeField]private float walkSpeed = 5.0f;
    [SerializeField] private float runSpeed = 7.5f;
    private float speed;
    private Vector3 moveDirection;
    private bool isRun => (Input.GetKey(KeyCode.LeftShift));

    [Header("Jump")]
    [SerializeField]private float jumpForce = 25.0f;
    [SerializeField]private float gravoty = -20.0f;
    bool groundedPlayer;

    [SerializeField] PlayerAnimations p_a;

    public bool graundHit => isOnGraund();

    //DEATH
    bool isAlive = true;
    [SerializeField] PlayerHelth playerHelth;
    //RagDoll
    [SerializeField] Rigidbody[] all_rb;
    [SerializeField] Collider[] all_cols;

    void Start()
    {
        cc = GetComponent<CharacterController>();
        playerHelth.onPlayersDeath.AddListener(Death);

        isAlive = true;
        cc.enabled = true;
        foreach (Rigidbody rb in all_rb) rb.isKinematic = true;
        foreach (Collider c in all_cols) c.enabled = false;
    }

    void Update()
    {
        if (isAlive)
        {
            MovePlayer();

        }

    }
    private void Death()
    {
        isAlive = false;
        cc.enabled = false;
        foreach (Rigidbody rb in all_rb) rb.isKinematic = false;
        foreach (Collider c in all_cols) c.enabled = true;
    }

    private void MovePlayer()
    {
        Vector3 move = Vector3.zero;
        if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
        {
            move += transform.forward * Input.GetAxis("Vertical");
            move += transform.right * Input.GetAxis("Horizontal");
            if (isRun) speed = runSpeed;
            else speed = walkSpeed;
        }
        if (Input.GetButtonDown("Jump") && isOnGraund())
        {
            StartCoroutine(CorJump());
        }
        //if on graund 
        if (isOnGraund())
        {
            cc.SimpleMove(move * speed);
        }

        else
        {
            moveDirection.x = move.x;
            moveDirection.z = move.z;
            moveDirection.y += gravoty * Time.deltaTime;
            cc.Move(moveDirection * Time.fixedDeltaTime);
        }
    }


    private IEnumerator CorJump()
    {
        moveDirection = Vector3.zero;
        moveDirection.y += Mathf.Sqrt(jumpForce * -3.0f * gravoty);
        p_a.PlayJumpAnim();

        float maxY = 0;


        for (int i = 0; i < 5; i++)
        {
            cc.Move(moveDirection * Time.fixedDeltaTime);
            yield return null;

            if (transform.position.y > maxY) maxY = transform.position.y;
        }

        //ADDED
        while (!isOnGraund())
        {
            cc.Move(moveDirection * Time.fixedDeltaTime);
            yield return null;
        }

    }

    private bool isOnGraund()
    {
        if (Physics.Raycast(transform.position, -transform.up, 1.1f))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

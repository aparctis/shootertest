using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CameraSpace;


public enum WeapoonType { shotgun, smg, ak}
public class Weapoon : MonoBehaviour
{
    [SerializeField] ParticleSystem partSis;
    private Transform barel;

    [Header("VALUES")]
    [SerializeField] WeaponStats stats;
    /*    [SerializeField] WeapoonType m_type;
        public WeapoonType type => m_type;
        [SerializeField] float damage = 25.0f;
        [SerializeField] float reloadTime = 0.2f;
        public float reload => reloadTime;
        [SerializeField] bool autoAble = true;
        public bool canAuto => autoAble;*/

    public WeapoonType type => stats.type;
    float damage => stats.damage;
    public float reload => stats.reload;
    public bool canAuto => stats.canAuto;
    LayerMask mask => stats.mask;

    float distance => stats.maxDistance;


    [Header("SPRITES")]
    [SerializeField] Sprite gunSprite;
    [SerializeField] Sprite amoSprite;
    public Sprite gun => gunSprite;
    public Sprite amo => amoSprite;

    [Header("AUDIO")]
    [SerializeField] private AudioClip sound;
    public AudioClip shootSound => sound;


    private void Awake()
    {
        barel = partSis.transform;
    }

    public void Shoot()
    {
        partSis.Play();

        Vector3 aimPoint = PlayerCameras.instance.aimPos();
        Vector3 direction = (aimPoint - barel.position).normalized;
        Ray ray = new Ray(barel.position, direction);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, distance, mask))
        {
            HitType hitType;
            if (hit.collider.gameObject.GetComponent<Enemy>() != null)
            {
                hit.collider.gameObject.GetComponent<Enemy>().GetDamage(damage, hit.point);
                hitType = HitType.blood;
                Debug.Log(damage);
            }

            else hitType = HitType.stone;

            HitPool.instance.HitCreate(hitType, hit.point, -direction);
            //Debug.Log(hit.collider.gameObject.name);
        }


    }





}

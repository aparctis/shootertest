using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Weapoons/Weapon stats")]
public class WeaponStats : ScriptableObject
{
    [Header("VALUES")]
    [SerializeField] WeapoonType m_type;
    public WeapoonType type => m_type;
    public float damage = 25.0f;
    [SerializeField] float reloadTime = 0.2f;
    public float reload => reloadTime;
    [SerializeField] bool autoAble = true;
    public bool canAuto => autoAble;

    [SerializeField] LayerMask m_mask;
    public LayerMask mask => m_mask;

    [SerializeField] float m_maxDistance;
    public float maxDistance => m_maxDistance;
}

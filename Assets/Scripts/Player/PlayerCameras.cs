using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace CameraSpace
{
    public class PlayerCameras : MonoBehaviour
    {
        [SerializeField] private Camera TPV_camera;
        [SerializeField] public Camera fPV_camera;
        [SerializeField] private Camera Other_camera;

        public Camera activeCamera { get; private set; }

        public bool isTPV { get; private set; }
        [SerializeField] PlayerAnimations anim;

        //events for camera change
        public static PlayerCameras instance;
        [HideInInspector]public UnityEvent set_fvp = new UnityEvent();
        [HideInInspector] public UnityEvent set_tvp = new UnityEvent();

        //msak
        [SerializeField] LayerMask mask;

        //FOR DEBUG
        [SerializeField] Transform aimPos_tr;

        bool isAlive = true;
        [SerializeField] PlayerHelth playerHelth;

        private void Awake()
        {
            if (instance == null) instance = this;
        }

        //TPV on START
        private void Start()
        {
            TPV_camera.enabled = true;

            fPV_camera.enabled = false;
            Other_camera.enabled = false;
            activeCamera = TPV_camera;

            isTPV = true;

            playerHelth.onPlayersDeath.AddListener(Death);

        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.V) && isAlive)
            {
                Debug.Log("Change view");
                if (isTPV) SetFPV();
                else SetTPV();
            }

            //debug
            //CaameraCheck();
        }

        private void Death()
        {
            isAlive = false;
            StartCoroutine(DeathCamera());
        }

        private void SetFPV()
        {
            fPV_camera.enabled = true;
            TPV_camera.enabled = false;
            Other_camera.enabled = false;
            activeCamera = fPV_camera;
            isTPV = false;

            set_fvp.Invoke();
        }



        private void SetTPV()
        {
            TPV_camera.enabled = true;
            fPV_camera.enabled = false;
            Other_camera.enabled = false;
            activeCamera = TPV_camera;

            isTPV = true;

            set_tvp.Invoke();
        }

        public Vector3 aimPos()
        {
            Ray ray = new Ray(activeCamera.transform.position, activeCamera.transform.forward);

            RaycastHit hit;
            Physics.Raycast(ray, out hit);
            //Physics.Raycast(ray, out hit);
            if (Physics.Raycast(ray, out hit, 3000.0f, mask))
            {

                return hit.point;
            }

            else
            {
                Vector3 simpleAim = (activeCamera.transform.position + (activeCamera.transform.forward * 50));
                return simpleAim;
            }

        }

        private IEnumerator DeathCamera()
        {
            Other_camera.transform.position = transform.position;
            Other_camera.transform.forward = Vector3.down;
            yield return null;
            Other_camera.enabled = true;

            TPV_camera.enabled = false;
            fPV_camera.enabled = false;
            float speed = 2.5f;
            Vector3 targetPlace = transform.position;
            targetPlace.y += 12.0f;

            while (Other_camera.transform.position != targetPlace)
            {
                Other_camera.transform.position = Vector3.MoveTowards(Other_camera.transform.position, targetPlace, speed * Time.deltaTime);
                yield return null;
            }

        }


        //DEBUG
        private void CaameraCheck()
        {
            Ray ray = new Ray(activeCamera.transform.position, activeCamera.transform.forward);
            RaycastHit hit;
            //Physics.Raycast(ray, out hit, layerMask);
            //Physics.Raycast(ray, out hit);
            if (Physics.Raycast(ray, out hit))
            {

                aimPos_tr.position = hit.point;
            }

            Debug.DrawLine(activeCamera.transform.position, hit.point, Color.red);

        }

        

    }

}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CameraSpace;

public class PLayerMouse : MonoBehaviour
{
    private Camera camera => PlayerCameras.instance.fPV_camera;

    //mouse rotation
    private float rotX;
    private float rotY;
    [SerializeField] private float sensativity = 1.0f;
    [SerializeField] Transform aimPoint;

    [Header("CONSTRAINTS")]
    private float minY_fpv = -35.0f;
    private float maxY_fpv = 60.0f;

/*    private float minY_tpv = -10.0f;
    private float maxY_tpv = 25.0f;*/

    private float minY;
    private float maxY;

    bool isAlive = true;
    [SerializeField] PlayerHelth playerHelth;

    private void Start()
    {
        playerHelth.onPlayersDeath.AddListener(Death);
    }

    private void Update()
    {
        if (isAlive)
        {
            OnMouseMove();

        }
    }

    private void Death()
    {
        isAlive = false;
    }

    private void OnMouseMove()
    {
        rotX += (Input.GetAxis("Mouse X")*sensativity);
        rotY -= (Input.GetAxis("Mouse Y") * sensativity);

        minY = minY_fpv;
        maxY = maxY_fpv;

        if (rotY < minY) rotY = minY;
        if (rotY > maxY) rotY = maxY;

        camera.transform.rotation = Quaternion.Euler(rotY, rotX, 0);
        transform.rotation = Quaternion.Euler(0, rotX, 0);

        //Vector3 defAimPointPos = camera.transform.position + (camera.transform.forward * 15);
        //aimPoint.transform.position = PlayerCameras.instance.aimPos();
    }





}

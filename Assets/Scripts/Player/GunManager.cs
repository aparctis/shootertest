using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CameraSpace;
using UnityEngine.UI;
using TMPro;


public class GunManager : MonoBehaviour
{
    [Header("0 = smg; 1 = shotgun; 2 = ak")]
    [SerializeField] List<Weapoon> fpv_guns = new List<Weapoon>();
    [SerializeField] List<Weapoon> tpv_guns = new List<Weapoon>();
    List<Weapoon> activeList;

    [SerializeField] GameObject fpv_gunholder;
    [SerializeField] GameObject tpv_gunholder;

    private int activeGunID = 0;
    private Weapoon activeWeapoon;

    [Header("UI")]
    [SerializeField] Image gunImage;
    [SerializeField] Image amonImage;
    [SerializeField] TextMeshProUGUI amoText;
    [SerializeField] Image addAmmonImage;
    [SerializeField] TextMeshProUGUI addAmmoText;
    Color invisible = new Color(0, 0, 0, 0);
    float pol = 1.0f;

    [SerializeField] AudioSource audioSource;
    private AudioClip activeShotSound;
    [SerializeField] private AudioClip reloadSound;
    [SerializeField] private AudioClip short_reloadSound;
    [SerializeField] AudioSource otherSounds;
    [SerializeField] private AudioClip amoAddSound;


    private bool isReloaded = true;

    //gun properties
    float reloadTime;
    bool canAuto;

    //AMO keeper
    //0 = amo in magazine left; 1 = amo in magazine max; 2 = total amo left
    private List<int> amo_ak_list = new List<int>(3) { 30, 30, 0 };
    private List<int> amo_uzi_list = new List<int>(3) { 20, 20, 0 };
    private List<int> amo_shotgan_list = new List<int>(3) { 8, 8, 0 };
    private List<int> active_amo_list;

    //for death
    bool isAlive = true;
    [SerializeField] PlayerHelth playerHelth;

    private void Start()
    {
        StartCoroutine(Suscriber());
    }

    private void Update()
    {
        if (isAlive)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                activeGunID--;
                if (activeGunID < 0) activeGunID = (2);
                ActivateWeapoon();
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                activeGunID++;
                if (activeGunID >2) activeGunID = 0;
                ActivateWeapoon();
            }

            if (Input.GetButtonDown("Fire1")) Shout();
            if (Input.GetButton("Fire1") && canAuto) Shout();
        }
    }

    private void OnDeath()
    {
        isAlive = false;
    }
    Coroutine amoshowCor = null;

    private void ActivateWeapoon()
    {
        if (isReloaded)
        {
            foreach (Weapoon w in fpv_guns)
            {
                w.gameObject.SetActive(false);
            }
            foreach (Weapoon w in tpv_guns)
            {
                w.gameObject.SetActive(false);
            }

            if (PlayerCameras.instance.isTPV) activeList = tpv_guns;
            else activeList = fpv_guns;

            activeWeapoon = activeList[activeGunID];
            activeWeapoon.gameObject.SetActive(true);
            gunImage.sprite = activeWeapoon.gun;
            amonImage.sprite = activeWeapoon.amo;
            reloadTime = activeWeapoon.reload;
            canAuto = activeWeapoon.canAuto;
            activeShotSound = activeWeapoon.shootSound;

            switch (activeWeapoon.type)
            {
                case WeapoonType.ak:
                    active_amo_list = amo_ak_list;
                    amoText.text = amoString(active_amo_list);

                    break;

                case WeapoonType.shotgun:
                    active_amo_list = amo_shotgan_list;
                    amoText.text = amoString(active_amo_list);

                    break;

                default:
                    active_amo_list = amo_uzi_list;
                    amoText.text = amoString(active_amo_list);

                    break;
            }

            if (active_amo_list[0] == 0 && active_amo_list[3] > 0) StartCoroutine(Reloader());
        }
    }

    private IEnumerator Suscriber()
    {
        while (PlayerCameras.instance == null) yield return null;
        PlayerCameras.instance.set_fvp.AddListener(ActivateWeapoon);
        PlayerCameras.instance.set_tvp.AddListener(ActivateWeapoon);
        yield return null;
        ActivateWeapoon();
    }

    private void Shout()
    {
        if (isReloaded && active_amo_list[0]>0)
        {
            audioSource.clip = activeShotSound;
            audioSource.Play();
            activeWeapoon.Shoot();
            isReloaded = false;
            active_amo_list[0]--;
            amoText.text = amoString(active_amo_list);
            StartCoroutine(Reloader());
        }

    }

    private IEnumerator Reloader()
    {

        if (!canAuto)
        {
            yield return new WaitForSeconds(reloadTime / 2);
            audioSource.clip = short_reloadSound;
            audioSource.Play();
            yield return new WaitForSeconds(reloadTime / 2);

        }

        else yield return new WaitForSeconds(reloadTime);



        if (active_amo_list[0] == 0)
        {
            audioSource.clip = reloadSound;
            audioSource.Play();
            yield return new WaitForSeconds(3.0f);
            if (active_amo_list[2] >= active_amo_list[1])
            {
                active_amo_list[2] -= active_amo_list[1];
                active_amo_list[0] += active_amo_list[1];
            }

            else
            {
                active_amo_list[0] += active_amo_list[2];
                active_amo_list[2] = 0;
            }
            amoText.text = amoString(active_amo_list);

        }

        isReloaded = true;
    }

    string amoString(List<int> amoList)
    {
        int magazine = amoList[0];
        int left = amoList[2];
        return (magazine +"\n/\n" + left);
    }


    public void AddAmo(WeapoonType type, int amoToAdd)
    {
        if (type == activeWeapoon.type)
        {
            active_amo_list[2] += amoToAdd;
            amoText.text = amoString(active_amo_list);
        }
        else
        {
            switch (type)
            {
                case WeapoonType.ak:
                    amo_ak_list[2] += amoToAdd;
                    break;
                case WeapoonType.shotgun:
                    amo_shotgan_list[2] += amoToAdd;
                    break;
                default:
                    amo_uzi_list[2] += amoToAdd;
                    break;
            }
        }

        if (active_amo_list[0] <= 0 && active_amo_list[2] > 0)
        {
            StartCoroutine(Reloader());
        }

        otherSounds.clip = amoAddSound;
        otherSounds.Play();


        if (amoshowCor != null) StopCoroutine(amoshowCor);
        amoshowCor = StartCoroutine(ShowNewAmmo(type, amoToAdd));
    }


    private IEnumerator ShowNewAmmo(WeapoonType type, int amoToAdd)
    {
        float pol = 0;
        Sprite ammoSprite = activeWeapoon.gun;
        foreach (Weapoon w in fpv_guns)
        {
            if (w.type == type) ammoSprite = w.amo;
        }
        addAmmonImage.sprite = ammoSprite;
        addAmmoText.text = ("+" + amoToAdd);

        while (pol < 1)
        {
            Color newCol = Color.Lerp(Color.green, invisible, pol);
            pol += Time.deltaTime/5;
            addAmmonImage.color = newCol;
            addAmmoText.color = newCol;
            yield return null;
        }
    }

}

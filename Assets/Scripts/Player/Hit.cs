using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HitType { blood, stone }
[RequireComponent(typeof(ParticleSystem))]
public class Hit : MonoBehaviour
{
    private ParticleSystem ps;

    [SerializeField] HitType type;

    private void Awake()
    {
        ps = GetComponent<ParticleSystem>();
    }

    private void OnEnable()
    {
        StartCoroutine(LifeCicle());
    }



    private IEnumerator LifeCicle()
    {
        ps.Play();
        while (ps.isPlaying) yield return null;
        HitPool.instance.BackToPool(this.gameObject, type);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPool : MonoBehaviour
{
    [SerializeField] GameObject hit_blood;
    [SerializeField] GameObject hit_stones;

    public static HitPool instance;

    private List<GameObject> pool_stone = new List<GameObject>();
    private List<GameObject> pool_blood = new List<GameObject>();

    private void Awake()
    {
        if (instance == null) instance = this;
        StartPool();
    }

    public void BackToPool(GameObject obj, HitType type)
    {
        obj.SetActive(false);
        switch (type)
        {
            case HitType.blood:
                pool_blood.Add(obj);
                break;
            default:
                pool_stone.Add(obj);
                break;
        }
    }

    public void HitCreate(HitType type, Vector3 place, Vector3 direction)
    {
        GameObject hitObj;
        switch (type)
        {
            case HitType.blood:
                if (pool_blood.Count > 0)
                {
                    hitObj = pool_blood[0];
                    pool_blood.RemoveAt(0);
                }
                else
                {
                    hitObj = Instantiate(hit_blood) as GameObject;
                    hitObj.SetActive(false);
                }
                break;
            default:
                if (pool_stone.Count > 0)
                {
                    hitObj = pool_stone[0];
                    pool_stone.RemoveAt(0);
                }
                else
                {
                    hitObj = Instantiate(hit_stones) as GameObject;
                    hitObj.SetActive(false);
                }
                break;
        }

        hitObj.transform.position = place;
        hitObj.transform.forward = direction;
        hitObj.SetActive(true);
    }

    private void StartPool()
    {
        for(int i = 0; i<50; i++)
        {
            GameObject ho_stone;
            ho_stone = Instantiate(hit_stones) as GameObject;
            ho_stone.SetActive(false);
            pool_stone.Add(ho_stone);

            GameObject ho_blood;
            ho_blood = Instantiate(hit_blood) as GameObject;
            ho_blood.SetActive(false);
            pool_blood.Add(ho_blood);

        }
    }

}

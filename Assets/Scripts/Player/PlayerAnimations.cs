using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;
using CameraSpace;

public class PlayerAnimations : MonoBehaviour
{
    //private CharacterController cc;
    private Animator anim;

    private bool isMove => (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0);
    private bool isRun => (Input.GetKey(KeyCode.LeftShift));

    private bool withWeapoon = false;
    private bool weapoonSwitching = false;

    [Header("AUDIO")]
    [SerializeField] private AudioSource stepAudioS;
    [SerializeField] AudioClip walkStepRight;
    [SerializeField] AudioClip walkStepLeft;

    [Header("PLAYER CONTROLLER")]
    [SerializeField] PlayerController pc;

    //DEATH
    bool isAlive = true;
    [SerializeField] PlayerHelth playerHelth;

    private void Start()
    {
        anim = GetComponent<Animator>();
        withWeapoon = anim.GetBool("isWithWeapoon");
        PlayerCameras.instance.set_fvp.AddListener(OnFVP);
        PlayerCameras.instance.set_tvp.AddListener(OnTVP);
        playerHelth.onPlayersDeath.AddListener(Death);
    }

    void Update()
    {
        if (isAlive)
        {
            if (isMove)
            {
                anim.SetBool("isMove", isMove);
                anim.SetBool("isRun", isRun);
                anim.SetFloat("Vertical", Input.GetAxis("Vertical"));
                anim.SetFloat("Horizontal", Input.GetAxis("Horizontal"));
            }
            else
            {
                anim.SetBool("isMove", isMove);

            }

            if (Input.GetKeyDown(KeyCode.B)) WithWeapoon();
        }

    }

    private void Death()
    {
        isAlive = false;
        anim.enabled = false;
    }

    public void PlayJumpAnim()
    {
        anim.SetTrigger("jump");
        Debug.Log("playjump");
    }

    private void OnTVP()
    {
        anim.SetBool("isTPV", true);
        Debug.Log("TVP SETED");
    }

    private void OnFVP()
    {
        anim.SetBool("isTPV", false);
        Debug.Log("FVP SETED");

    }


    #region AUDIO
    //wak
    public void StepWalkRight()
    {
        if (!stepAudioS.isPlaying && pc.graundHit|| stepAudioS.isPlaying&& stepAudioS.clip != walkStepRight && pc.graundHit)
        {
            stepAudioS.clip = walkStepRight;
            stepAudioS.Play();
        }

    }
    public void StepWalkLeft()
    {
        if (!stepAudioS.isPlaying && pc.graundHit || stepAudioS.isPlaying && stepAudioS.clip != walkStepLeft && pc.graundHit)
        {
            stepAudioS.clip = walkStepLeft;
            stepAudioS.Play();
        }

    }
    //run
    public void StepRunLeft()
    {
        if (!stepAudioS.isPlaying && pc.graundHit || stepAudioS.isPlaying && stepAudioS.clip != walkStepLeft && pc.graundHit)
        {
            stepAudioS.clip = walkStepLeft;
            stepAudioS.Play();
        }

    }
    public void StepRunRight()
    {
        if (!stepAudioS.isPlaying && pc.graundHit || stepAudioS.isPlaying && stepAudioS.clip != walkStepRight && pc.graundHit)
        {
            stepAudioS.clip = walkStepRight;
            stepAudioS.Play();
        }

    }



    #endregion

    private void WithWeapoon()
    {
        if (!weapoonSwitching)
        {
            Debug.Log("Start switch");
            if (withWeapoon)
            {
                Debug.Log("HIDE");

                withWeapoon = false;
                StartCoroutine(WeapoonInArms(withWeapoon));
            }

            else
            {
                Debug.Log("SHOW");

                withWeapoon = true;
                StartCoroutine(WeapoonInArms(withWeapoon));

            }
        }
        else
        {
            Debug.Log("mistake");

        }
    }
    private IEnumerator WeapoonInArms(bool takeWeapoon)
    {
        weapoonSwitching = true;



        float weapoonSpeed = 0.05f;
        if (takeWeapoon)
        {
            float ww = 0;
            while (ww < 1.0f)
            {
                ww += weapoonSpeed;
                anim.SetFloat("WW_float", ww);


                yield return null;
            }
            anim.SetBool("isWithWeapoon", true);
            anim.SetFloat("WW_float", 1.0f);
        }

        else
        {

            float ww = 1.0f;
            while (ww >0f)
            {
                ww -= weapoonSpeed;
                anim.SetFloat("WW_float", ww);

                yield return null;
            }
            anim.SetBool("isWithWeapoon", false);
            anim.SetFloat("WW_float", 0f);

        }
        weapoonSwitching = false;
    }



}

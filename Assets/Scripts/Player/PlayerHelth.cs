using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class PlayerHelth : MonoBehaviour
{

    [SerializeField] Slider slider;
    [SerializeField] TextMeshProUGUI helth_text;
    [SerializeField] int max_health = 100;
    [SerializeField] Image ava;
    [SerializeField] Image slider_fill;

    float helthleft;
    float maxF;
    [HideInInspector] public UnityEvent onPlayersDeath;
    [HideInInspector] public UnityEvent onLevelWin;

    [Header("AUDIO")]
    [SerializeField] AudioClip punchClip;
    [SerializeField] AudioClip deathClip;
    [SerializeField] AudioSource otherAudio;

    private void Start()
    {
        HelthInit();
    }

    //DEBUG
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M)) Damaged(10.0f);
    }

    private void HelthInit()
    {
        helthleft = max_health;
        slider.maxValue = max_health;
        slider.value = helthleft;
        maxF = max_health;
        HelthUpdtate();

    }

    private void HelthUpdtate()
    {
        int left = Mathf.RoundToInt(helthleft);
        helth_text.text = (left + "/" + max_health);
        float pol = (1 - (helthleft / maxF));
        Color sliderCol = Color.Lerp(Color.white, Color.red, pol);
        slider.value = helthleft;
        slider_fill.color = sliderCol;
    }


    public void Damaged(float dam)
    {
        helthleft -= dam;
        if (helthleft < 0)
        {
            otherAudio.clip = deathClip;
            otherAudio.Play();
            helthleft = 0;
            onPlayersDeath.Invoke();
        }
        else
        {
            otherAudio.clip = punchClip;
            otherAudio.Play();
        }

        StartCoroutine(VisualDamage());
        HelthUpdtate();
    }

    private IEnumerator VisualDamage()
    {
        ava.color = Color.red;
        helth_text.color = Color.red;
        yield return new WaitForSeconds(0.15f);
        ava.color = Color.white;
        helth_text.color = Color.white;

    }

    public void LevelWin()
    {
        onLevelWin.Invoke();
    }
}
